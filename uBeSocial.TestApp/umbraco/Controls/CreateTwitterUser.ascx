﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateTwitterUser.ascx.cs" Inherits="uBeSocial.umbraco.Controls.CreateTwitterUser" %>
<div style="margin-top: 20px">
    Name:
    <asp:RequiredFieldValidator ErrorMessage="*" ControlToValidate="UserName" runat="server">*</asp:RequiredFieldValidator>
    <br />
    <asp:TextBox ID="UserName" runat="server" CssClass="bigInput" Width="300px" />
    <br />
    <br />

    Consumer Key:
    <asp:RequiredFieldValidator ErrorMessage="*" ControlToValidate="ConsumerKey" runat="server">*</asp:RequiredFieldValidator>
    <br />
    <asp:TextBox ID="ConsumerKey" runat="server" CssClass="bigInput" Width="300px" />
    <br />
    <br />

    Consumer Secret:
    <asp:RequiredFieldValidator ErrorMessage="*" ControlToValidate="ConsumerSecret" runat="server">*</asp:RequiredFieldValidator>
    <br />
    <asp:TextBox ID="ConsumerSecret" runat="server" CssClass="bigInput" Width="300px" />
    <br />
    <br />

    Access Token:
    <asp:RequiredFieldValidator ErrorMessage="*" ControlToValidate="AccessToken" runat="server">*</asp:RequiredFieldValidator>
    <br />
    <asp:TextBox ID="AccessToken" runat="server" CssClass="bigInput" Width="300px" />
    <br />
    <br />

    Access Token Secret:
    <asp:RequiredFieldValidator ErrorMessage="*" ControlToValidate="AccessTokenSecret" runat="server">*</asp:RequiredFieldValidator>
    <br />
    <asp:TextBox ID="AccessTokenSecret" runat="server" CssClass="bigInput" Width="300px" />
    <br />
    <br />
</div>
<div style="padding-top: 25px;">
    <asp:Button ID="CreateButton" runat="server" Style="width: 90px" OnClick="CreateButton_OnClick" Text="Create"></asp:Button>
    &nbsp; <em>or</em> &nbsp;
  <a href="#" style="color: blue" onclick="UmbClientMgr.closeModalWindow()">Cancel</a>
</div>
