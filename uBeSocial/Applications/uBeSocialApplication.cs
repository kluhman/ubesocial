﻿using System.Collections.Generic;
using uBeSocial.Models;
using umbraco.businesslogic;
using umbraco.interfaces;

namespace uBeSocial.Applications
{
    [Application("uBeSocial", "uBeSocial", "uBeSocial-icon.png", 15)]
    public class UBeSocialApplication : IApplication
    {
        private static readonly List<NetworkPosterRegistration> _registrations = new List<NetworkPosterRegistration>();
        internal static IReadOnlyList<NetworkPosterRegistration> Registrations => _registrations;

        public static void RegisterNetworkPoster(NetworkPosterRegistration poster)
        {
            _registrations.Add(poster);
        }
    }
}