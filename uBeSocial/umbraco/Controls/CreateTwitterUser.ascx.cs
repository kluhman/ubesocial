﻿using System;
using System.Web.UI;
using uBeSocial.Helpers;
using uBeSocial.Models;
using uBeSocial.Services.Interfaces;
using umbraco.BasePages;

namespace uBeSocial.umbraco.Controls
{
    public partial class CreateTwitterUser : UserControl
    {
        private readonly ITwitterUserService _twitterUserService;

        public CreateTwitterUser()
        {
            _twitterUserService = DependencyResolver.Instance.Resolve<ITwitterUserService>();
        }

        protected void CreateButton_OnClick(object sender, EventArgs e)
        {
            var user = new TwitterUserAuthorization
            {
                UserName = UserName.Text,
                AccessToken = AccessToken.Text,
                AccessTokenSecret = AccessTokenSecret.Text,
                ConsumerKey = ConsumerKey.Text,
                ConsumerSecret = ConsumerSecret.Text
            };

            _twitterUserService.Create(user);

            BasePage.Current.ClientTools.ReloadActionNode(false, true);
            BasePage.Current.ClientTools.CloseModalWindow();
            BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.save, "Saved", "User created successfully!");
        }
    }
}