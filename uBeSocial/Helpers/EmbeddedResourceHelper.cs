﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace uBeSocial.Helpers
{
    internal static class EmbeddedResourceHelper
    {
        public static Stream GetEmbeddedResource(Assembly assembly, string resourceName)
        {
            return assembly.GetManifestResourceStream(resourceName);
        }

        public static string GetEmbeddedResourceName(Assembly assembly, string virtualPath)
        {
            virtualPath = virtualPath.TrimStart('~');
            return string.Concat(assembly.GetName().Name, virtualPath.Replace("/", "."));
        }
    }
}