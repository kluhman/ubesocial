﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uBeSocial.Services;
using uBeSocial.Services.Interfaces;
using Umbraco.Core;
using Umbraco.Core.Persistence;

namespace uBeSocial.Helpers
{
    internal class DependencyResolver
    {
        private readonly IContainer _container;

        private static DependencyResolver _instance;
        private static readonly object _instanceLock = new object();

        public DependencyResolver()
        {
            _container = BuildContainer();
        }

        public T Resolve<T>()
        {
            using (var scope = _container.BeginLifetimeScope())
            {
                return scope.Resolve<T>();
            }
        }

        public static DependencyResolver Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_instanceLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new DependencyResolver();
                        }
                    }
                }

                return _instance;
            }
        }

        private static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder.Register<Database>(c => ApplicationContext.Current.DatabaseContext.Database).As<Database>();
            builder.RegisterType<TwitterService>().As<ITwitterService>();
            builder.RegisterType<TwitterUserService>().As<ITwitterUserService>();
            builder.RegisterType<TwitterConfigurationService>().As<ITwitterConfigurationService>();

            return builder.Build();
        }
    }
}
