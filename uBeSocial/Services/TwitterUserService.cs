﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uBeSocial.Extensions;
using uBeSocial.Models;
using uBeSocial.Services.Interfaces;
using Umbraco.Core.Persistence;

namespace uBeSocial.Services
{
    internal class TwitterUserService : ITwitterUserService
    {
        private readonly Database _database;

        public TwitterUserService(Database database)
        {
            _database = database;
            Initialize();
        }

        public void Create(TwitterUserAuthorization user)
        {
            _database.Insert(user);
        }

        public List<TwitterUserAuthorization> Get()
        {
            var sql = new Sql().Select("*").From<TwitterUserAuthorization>();
            return _database.Fetch<TwitterUserAuthorization>(sql);
        }

        public TwitterUserAuthorization Get(int id)
        {
            var sql = new Sql().Select("*").From<TwitterUserAuthorization>().Where<TwitterUserAuthorization>(u => u.Id == id);
            return _database.Fetch<TwitterUserAuthorization>(sql).FirstOrDefault();
        }

        public int Update(TwitterUserAuthorization user)
        {
            return _database.Update(user);
        }

        public int Delete(int id)
        {
            return _database.Delete<TwitterUserAuthorization>(id);
        }

        private void Initialize()
        {
            if (_database.TableExist<TwitterUserAuthorization>())
            {
                return;
            }
            
            _database.CreateTable<TwitterUserAuthorization>();
        }
    }
}
