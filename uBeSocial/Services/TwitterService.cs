﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using LinqToTwitter;
using uBeSocial.Services.Interfaces;

namespace uBeSocial.Services
{
    internal class TwitterService : ITwitterService
    {
        private readonly ITwitterUserService _twitterUserService;
        private readonly ITwitterConfigurationService _twitterConfigurationService;

        public TwitterService(ITwitterUserService twitterUserService, ITwitterConfigurationService twitterConfigurationService)
        {
            _twitterUserService = twitterUserService;
            _twitterConfigurationService = twitterConfigurationService;
        }

        public void Post(string tweet)
        {
            var context = new TwitterContext(GetAuthorizer());
            context.TweetAsync(tweet).Wait();
        }

        private IAuthorizer GetAuthorizer()
        {
            var configuration = _twitterConfigurationService.Get();
            var user = configuration.TwitterUserId.HasValue ? _twitterUserService.Get(configuration.TwitterUserId.Value) : null;

            if (user == null)
            {
                throw new AuthenticationException("No user account selected for authorization");
            }

            return new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore
                {
                    ConsumerKey = user.ConsumerKey,
                    ConsumerSecret = user.ConsumerSecret,
                    AccessToken = user.AccessToken,
                    AccessTokenSecret = user.AccessTokenSecret
                }
            };
        }
    }
}
