﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uBeSocial.Services.Interfaces
{
    internal interface ITwitterService
    {
        void Post(string tweet);
    }
}
