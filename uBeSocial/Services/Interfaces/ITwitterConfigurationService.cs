﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uBeSocial.Models;

namespace uBeSocial.Services.Interfaces
{
    internal interface ITwitterConfigurationService
    {
        TwitterConfiguration Get();
        void Update(TwitterConfiguration configuration);
    }
}
