﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uBeSocial.Models;

namespace uBeSocial.Services.Interfaces
{
    internal interface ITwitterUserService
    {
        void Create(TwitterUserAuthorization user);
        List<TwitterUserAuthorization> Get();
        TwitterUserAuthorization Get(int id);
        int Update(TwitterUserAuthorization user);
        int Delete(int id);
    }
}
