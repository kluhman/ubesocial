﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uBeSocial.Extensions;
using uBeSocial.Models;
using uBeSocial.Services.Interfaces;
using Umbraco.Core.Persistence;

namespace uBeSocial.Services
{
    internal class TwitterConfigurationService : ITwitterConfigurationService
    {
        private readonly Database _database;

        public TwitterConfigurationService(Database database)
        {
            _database = database;
            Initialize();
        }

        public TwitterConfiguration Get()
        {
            var sql = new Sql().Select("*").From<TwitterConfiguration>();
            return _database.FirstOrDefault<TwitterConfiguration>(sql) ?? new TwitterConfiguration();
        }

        public void Update(TwitterConfiguration configuration)
        {
            if (configuration.Id > 0)
            {
                _database.Update(configuration);
            }
            else
            {
                _database.Insert(configuration);
            }
        }

        private void Initialize()
        {
            if (_database.TableExist<TwitterConfiguration>())
            {
                return;
            }

            _database.CreateTable<TwitterConfiguration>();
        }
    }
}
