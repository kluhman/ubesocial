﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;

namespace uBeSocial.Extensions
{
    internal static class PetaPocoExtensions
    {
        public static bool TableExist<T>(this Database db)
        {
            return db.TableExist(GetTableName<T>());
        }

        public static string GetTableName<T>()
        {
            var attribute = typeof(T).GetCustomAttributes(typeof(TableNameAttribute), false).FirstOrDefault() as TableNameAttribute;
            return attribute?.Value ?? typeof(T).Name;
        }
    }
}
