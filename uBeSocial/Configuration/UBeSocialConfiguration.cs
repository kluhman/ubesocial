﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace uBeSocial.Configuration
{
    internal class UBeSocialConfiguration
    {
        private const string UBeSocialSettingPrefix = "uBeSocial.";
        private static readonly Dictionary<string, object> _cache = new Dictionary<string, object>();

        public static class Facebook
        {
            private const string FacebookSettingPrefix = UBeSocialSettingPrefix + "Facebook.";

            public static string AppId => GetValueOrDefault<string>(FacebookSettingPrefix + "AppId");
        }

        public static class Pinterest
        {
            private const string PinterestSettingPrefix = UBeSocialSettingPrefix + "Pinterest.";

            public static string AppId => GetValueOrDefault<string>(PinterestSettingPrefix + "AppId");
        }

        public static class Twitter
        {
            private const string TwitterSettingPrefix = UBeSocialSettingPrefix + "Twitter.";

            public static bool EnableTwitterPoster => GetValueOrDefault<bool>(TwitterSettingPrefix + "EnableTwitterPoster");
        }

        public static T GetValueOrDefault<T>(string key, T defaultValue = default(T))
        {
            if (_cache.ContainsKey(key))
            {
                return (T)_cache[key];
            }

            T value = defaultValue;
            lock (new object())
            {
                if (_cache.ContainsKey(key))
                {
                    value = (T)_cache[key];
                }
                else
                {
                    try
                    {
                        var strValue = ConfigurationManager.AppSettings[key];
                        if (strValue != null)
                        {
                            value = (T)Convert.ChangeType(strValue, typeof(T));
                        }
                    }
                    finally
                    {
                        _cache.Add(key, value);
                    }
                }
            }

            return value;
        }
    }
}
