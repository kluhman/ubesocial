﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace uBeSocial.ViewModels
{
    public class TwitterDashboardViewModel : BaseViewModel
    {
        public int UserId { get; set; }

        public List<SelectListItem> Users { get; set; }
    }
}
