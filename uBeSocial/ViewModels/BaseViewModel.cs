﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uBeSocial.ViewModels
{
    public abstract class BaseViewModel
    {
        public string Version => GetType().Assembly.GetName().Version.ToString();
    }
}
