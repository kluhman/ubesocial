﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uBeSocial.ViewModels
{
    public class DashboardViewModel : BaseViewModel
    {
        public List<string> Dependencies { get; set; }

        public List<string> Posters { get; set; }

        public Dictionary<string, object> Settings { get; set; }
    }
}
