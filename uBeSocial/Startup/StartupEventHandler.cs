﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using System.Web.Routing;
using uBeSocial.Applications;
using uBeSocial.Configuration;
using uBeSocial.Models;
using uBeSocial.Startup.Configs;
using uBeSocial.VirtualProviders;

using Umbraco.Core;

namespace uBeSocial.Startup
{
    public class StartupEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            StartupConfig.RegisterDefaultPosters();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            HostingEnvironment.RegisterVirtualPathProvider(new EmbeddedResourceProvider());
        }
    }
}