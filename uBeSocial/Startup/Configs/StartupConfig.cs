﻿using System;
using System.Collections.Generic;
using System.Linq;
using uBeSocial.Applications;
using uBeSocial.Configuration;
using uBeSocial.Models;

namespace uBeSocial.Startup.Configs
{
    internal static class StartupConfig
    {
        public static void RegisterDefaultPosters()
        {
            RegisterFacebookPoster();
            RegisterPinterestPoster();
            RegisterTwitterPoster();
        }

        private static void RegisterFacebookPoster()
        {
            if (!string.IsNullOrWhiteSpace(UBeSocialConfiguration.Facebook.AppId))
            {
                UBeSocialApplication.RegisterNetworkPoster(new NetworkPosterRegistration
                {
                    Dependencies = new List<string> { "//connect.facebook.net/en_US/sdk.js" },
                    Poster = "~/UBeSocialStaticResources/scripts/posters/FacebookPoster.js",
                    SettingsName = "FACEBOOK",
                    Settings = new
                    {
                        AppId = UBeSocialConfiguration.Facebook.AppId
                    }
                });
            }
        }

        private static void RegisterPinterestPoster()
        {
            if (!string.IsNullOrWhiteSpace(UBeSocialConfiguration.Pinterest.AppId))
            {
                UBeSocialApplication.RegisterNetworkPoster(new NetworkPosterRegistration
                {
                    Dependencies = new List<string> { "//assets.pinterest.com/sdk/sdk.js" },
                    Poster = "~/UBeSocialStaticResources/scripts/posters/PinterestPoster.js",
                    SettingsName = "PINTEREST",
                    Settings = new
                    {
                        AppId = UBeSocialConfiguration.Pinterest.AppId
                    }
                });
            }
        }

        private static void RegisterTwitterPoster()
        {
            if (UBeSocialConfiguration.Twitter.EnableTwitterPoster)
            {
                UBeSocialApplication.RegisterNetworkPoster(new NetworkPosterRegistration
                {
                    Dependencies = new List<string>(),
                    Poster = "~/UBeSocialStaticResources/scripts/posters/TwitterPoster.js"
                });
            }
        }
    }
}
