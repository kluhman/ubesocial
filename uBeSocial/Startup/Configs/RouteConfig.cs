﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace uBeSocial.Startup.Configs
{
    internal static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "uBeSocial",
                "umbraco/uBeSocial/{controller}/{action}",
                new { controller = "UBeSocialDashboard", action = "Index" }
                );
        }
    }
}