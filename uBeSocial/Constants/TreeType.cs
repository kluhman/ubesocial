﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uBeSocial.Constants
{
    internal class TreeType
    {
        public const string MediaTree = "media";
        public const string ContentTree = "content";
    }
}
