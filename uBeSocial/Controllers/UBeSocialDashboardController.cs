﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using uBeSocial.Applications;
using uBeSocial.Constants;
using uBeSocial.Models;
using uBeSocial.ViewModels;
using umbraco;
using Umbraco.Web.Mvc;

namespace uBeSocial.Controllers
{
    public class UBeSocialDashboardController : UmbracoAuthorizedController
    {
        private const int RootTreeId = -1;
        private const string UmbracoFileProperty = "umbracoFile";

        public ActionResult Index()
        {
            var model = new DashboardViewModel
            {
                Dependencies = UBeSocialApplication.Registrations.SelectMany(r => r.Dependencies ?? new List<string>()).Where(d => !string.IsNullOrWhiteSpace(d)).ToList(),
                Posters = UBeSocialApplication.Registrations.Select(r => r.Poster).Where(p => !string.IsNullOrWhiteSpace(p)).ToList(),
                Settings = UBeSocialApplication.Registrations.Where(r => !string.IsNullOrWhiteSpace(r.SettingsName) && r.Settings != null).ToDictionary(r => r.SettingsName, r => r.Settings)
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult GetChildNodes(string treeType, int id = RootTreeId)
        {
            IEnumerable<TreeNode> nodes;
            try
            {
                switch (treeType)
                {
                    case TreeType.ContentTree:
                        var content = id == RootTreeId ? Umbraco.TypedContentAtRoot() : Umbraco.TypedContent(id).Children;
                        nodes = content.Select(c => new TreeNode { Id = c.Id, Name = c.Name, Url = Umbraco.UrlAbsolute(c.Id) });
                        break;
                    case TreeType.MediaTree:
                        var media = id == RootTreeId ? Umbraco.TypedMediaAtRoot() : Umbraco.TypedMedia(id).Children;
                        nodes = media.Select(m => new TreeNode { Id = m.Id, Name = m.Name, Url = m.GetProperty(UmbracoFileProperty) == null ? null : GetAbsoluteUrl(m.Url) });
                        break;
                    default:
                        nodes = new List<TreeNode>();
                        break;
                }
            }
            catch
            {
                nodes = new List<TreeNode>();
            }

            return Json(nodes, JsonRequestBehavior.AllowGet);
        }

        private string GetAbsoluteUrl(string url)
        {
            if (Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                return url;
            }

            var domain = Request.Url.GetLeftPart(UriPartial.Authority);
            return domain.TrimEnd('/') + '/' + url.TrimStart('/');
        }
    }
}