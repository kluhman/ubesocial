﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using uBeSocial.Services.Interfaces;
using uBeSocial.ViewModels;
using Umbraco.Core.Logging;
using Umbraco.Web.Mvc;

namespace uBeSocial.Controllers
{
    public class UBeSocialTwitterController : UmbracoAuthorizedController
    {
        private readonly ITwitterService _twitterService;
        private readonly ITwitterUserService _twitterUserService;
        private readonly ITwitterConfigurationService _twitterConfigurationService;

        public UBeSocialTwitterController()
        {
            _twitterService = Helpers.DependencyResolver.Instance.Resolve<ITwitterService>();
            _twitterUserService = Helpers.DependencyResolver.Instance.Resolve<ITwitterUserService>();
            _twitterConfigurationService = Helpers.DependencyResolver.Instance.Resolve<ITwitterConfigurationService>();
        }

        public ActionResult Index()
        {
            var users = _twitterUserService.Get().Select(u => new SelectListItem { Text = u.UserName, Value = u.Id.ToString() }).ToList();
            users.Insert(0, new SelectListItem { Text = "Select a user", Value = string.Empty });

            var model = new TwitterDashboardViewModel
            {
                UserId = _twitterConfigurationService.Get()?.TwitterUserId ?? 0,
                Users = users
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(TwitterDashboardViewModel model)
        {
            var configuration = _twitterConfigurationService.Get();
            configuration.TwitterUserId = model.UserId;

            _twitterConfigurationService.Update(configuration);
            TempData["Success"] = "Successfully updated active user";

            return RedirectToAction("Index");
        }

        public ActionResult User(int id)
        {
            var user = _twitterUserService.Get(id);
            if (user == null)
            {
                return HttpNotFound($"No user with id '{id}' was found");
            }

            var model = new TwitterUserViewModel
            {
                Id = id,
                UserName = user.UserName,
                ConsumerKey = user.ConsumerKey,
                ConsumerSecret = user.ConsumerSecret,
                AccessToken = user.AccessToken,
                AccessTokenSecret = user.AccessTokenSecret
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult User(TwitterUserViewModel model)
        {
            var user = _twitterUserService.Get(model.Id);
            user.ConsumerKey = model.ConsumerKey;
            user.ConsumerSecret = model.ConsumerSecret;
            user.AccessToken = model.AccessToken;
            user.AccessTokenSecret = model.AccessTokenSecret;

            _twitterUserService.Update(user);

            TempData["Success"] = "Successfully updated user.";

            return RedirectToAction("User", new { id = user.Id });
        }

        [HttpPost]
        public ActionResult Tweet(string tweet)
        {
            try
            {
                _twitterService.Post(tweet);
            }
            catch (Exception ex)
            {
                LogHelper.Error<UBeSocialTwitterController>("An unexpected error occurred posting to Twitter", ex);
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(new { Success = true, Message = "Successfully posted status to Twitter" });
        }
    }
}
