﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uBeSocial.Models
{
    /// <summary>
    ///     Encapsulates network poster registration
    /// </summary>
    public class NetworkPosterRegistration
    {
        /// <summary>
        ///     Scripts network poster is dependent on. Can be virtual (&quot;~/Scripts/...&quot;) or absolute (&quot;//connect.facebook.com&quot;) 
        /// </summary>
        public List<string> Dependencies { get; set; } = new List<string>();

        /// <summary>
        ///     Script containing network poster. Can be virtual (&quot;~/Scripts/...&quot;) or absolute (&quot;//connect.facebook.com&quot;) 
        /// </summary>
        public string Poster { get; set; } 

        /// <summary>
        ///     Name of settings property on global JS SETTINGS object
        /// </summary>
        public string SettingsName { get; set; }

        /// <summary>
        ///     Settings object required to configure network poster. Settings will be available on global JS SETTINGS object (window.SETTINGS[SettingsName])
        /// </summary>
        public object Settings { get; set; }
    }
}
