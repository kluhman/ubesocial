﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace uBeSocial.Models
{
    [PrimaryKey("Id")]
    [TableName("uBeSocial_TwitterConfiguration")]
    internal class TwitterConfiguration
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        public int? TwitterUserId { get; set; }
    }
}
