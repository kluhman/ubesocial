﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace uBeSocial.Models
{
    [PrimaryKey("Id")]
    [TableName("uBeSocial_TwitterUsers")]
    internal class TwitterUserAuthorization
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [SpecialDbType(SpecialDbTypes.NTEXT)]
        public string UserName { get; set; }

        public string ConsumerKey { get; set; }

        public string ConsumerSecret { get; set; }

        public string AccessToken { get; set; }

        public string AccessTokenSecret { get; set; }
    }
}
