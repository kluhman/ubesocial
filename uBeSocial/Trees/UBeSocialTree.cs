﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uBeSocial.Helpers;
using uBeSocial.Services.Interfaces;
using umbraco.businesslogic;
using umbraco.BusinessLogic.Actions;
using umbraco.cms.presentation.Trees;
using umbraco.interfaces;

namespace uBeSocial.Trees
{
    [Tree("uBeSocial", "uBeSocial", "uBeSocial")]
    public class UBeSocialTree : BaseTree
    {
        private readonly ITwitterUserService _twitterUserService;

        private const string UBeSocialDashboardAction = "/umbraco/uBeSocial/";

        public UBeSocialTree(string application) : base(application)
        {
            _twitterUserService = DependencyResolver.Instance.Resolve<ITwitterUserService>();
        }

        public override void RenderJS(ref StringBuilder javascript)
        {
            javascript.Append(@"function openPage(url) {UmbClientMgr.contentFrame(url); }");
        }

        public override void Render(ref XmlTree tree)
        {
            switch (NodeKey)
            {
                case "Twitter":
                    var users = _twitterUserService.Get();
                    foreach (var user in users)
                    {
                        var userNode = CreateNode($"Twitter|{user.Id}", user.UserName, $"/umbraco/uBeSocial/UBeSocialTwitter/User?id={user.Id}", false, ActionDelete.Instance);
                        userNode.NodeType = "uBeSocialTwitterUser";
                        tree.Add(userNode);
                    }
                    break;
                default:
                    var twitterNode = CreateNode("Twitter", "Twitter", "/umbraco/uBeSocial/UBeSocialTwitter", true, ActionNew.Instance, ActionRefresh.Instance);
                    twitterNode.NodeType = "uBeSocialTwitterUser";
                    tree.Add(twitterNode);
                    break;
            }
        }

        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.Text = "uBeSocial Dashboard";
            rootNode.NodeType = "root";
            rootNode.NodeID = "0";
            rootNode.Action = $"javascript:openPage('{UBeSocialDashboardAction}');";
            rootNode.Menu = new List<IAction> { ActionRefresh.Instance };
        }

        private XmlTreeNode CreateNode(string nodeId, string nodeTitle, string nodeAction, bool hasChildren, params IAction[] actions)
        {
            var node = XmlTreeNode.Create(this);
            node.NodeID = nodeId;
            node.Text = nodeTitle;
            node.Action = $"javascript:openPage('{nodeAction}');";
            node.Icon = hasChildren ? ".sprTreeFolder" : ".sprTreeDoc";
            node.OpenIcon = ".sprTreeFolder_o";
            node.Menu = actions.ToList();

            if (hasChildren)
            {
                var treeService = new TreeService(-1, TreeAlias, ShowContextMenu, IsDialog, DialogMode, app, nodeId);
                node.HasChildren = true;
                node.Source = treeService.GetServiceUrl();
            }

            return node;
        }
    }
}
