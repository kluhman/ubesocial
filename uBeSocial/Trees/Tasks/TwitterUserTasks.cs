﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uBeSocial.Helpers;
using uBeSocial.Services.Interfaces;
using umbraco.interfaces;

namespace uBeSocial.Trees.Tasks
{
    public class TwitterUserTasks : ITaskReturnUrl
    {
        private readonly ITwitterUserService _twitterUserService;

        public TwitterUserTasks()
        {
            _twitterUserService = DependencyResolver.Instance.Resolve<ITwitterUserService>();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public bool Delete()
        {
            var userId = int.Parse(Alias.Split('|').Skip(1).First());
            return _twitterUserService.Delete(userId) > 0;
        }

        public int ParentID { get; set; }
        public int TypeID { get; set; }
        public string Alias { get; set; }
        public int UserId { get; set; }
        public string ReturnUrl { get; set; }
    }
}
