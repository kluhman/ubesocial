﻿var uBeSocial = uBeSocial || {};
uBeSocial.Views = uBeSocial.Views || {};

(function (uBeSocial, $) {
    var SELECTORS = {
        REFRESH: '[data-treenode-role=refresh]',
        CHILDREN: '[data-treenode-role=children]',
        EXPAND_BUTTON: '[data-treenode-role=expand]',
        SELECT_BUTTON: '[data-treenode-role=select]',
        TREE_NODE_TEMPLATE: '[data-treenode-role=template]'
    };

    uBeSocial.Views.TreeNodeView = function ($element, treeNode, treeType, selectedPromise) {
        this.init($element, treeNode, treeType, selectedPromise)
            .setupHandlers()
            .createChildren()
            .enable()
            .draw();
    }
    
    uBeSocial.Views.TreeNodeView.prototype.init = function ($element, treeNode, treeType, selectedPromise) {
        this.$element = $element;
        this.treeNode = treeNode;
        this.treeType = treeType;
        this.selectedPromise = selectedPromise;

        this.isExpanded = false;
        this.childrenLoaded = false;

        return this;
    };

    uBeSocial.Views.TreeNodeView.prototype.setupHandlers = function () {
        this.nodeSelected = this.nodeSelected.bind(this);
        this.toggleChildren = this.toggleChildren.bind(this);

        return this;
    };

    uBeSocial.Views.TreeNodeView.prototype.createChildren = function () {
        this.$refreshIcon = this.$element.find(SELECTORS.REFRESH);
        this.$childrenContainer = this.$element.find(SELECTORS.CHILDREN);
        this.$expandButton = this.$element.find(SELECTORS.EXPAND_BUTTON);
        this.$selectNodeButton = this.$element.find(SELECTORS.SELECT_BUTTON);
        this.$treeNodeTemplate = $(SELECTORS.TREE_NODE_TEMPLATE).children().first();

        return this;
    };

    uBeSocial.Views.TreeNodeView.prototype.enable = function () {
        this.$expandButton.on('click', this.toggleChildren);
        this.$selectNodeButton.on('click', this.nodeSelected);

        return this;
    };

    uBeSocial.Views.TreeNodeView.prototype.draw = function () {
        this.$refreshIcon.hide();
        this.$selectNodeButton.text(this.treeNode.Name);
        if (this.treeNode.Url == null) {
            this.$selectNodeButton.addClass('disabled');
            this.$selectNodeButton.prop('disabled', true);
        }
    };

    uBeSocial.Views.TreeNodeView.prototype.toggleChildren = function () {
        var self = this;
        
        if (!this.childrenLoaded) {
            this.$refreshIcon.fadeIn();
            uBeSocial.Utils.TreeApi.getChildNodes(this.treeType, this.treeNode.Id).done(function (nodes) {
                nodes.forEach(function (node) {
                    var html = self.$treeNodeTemplate.clone();
                    var view = new uBeSocial.Views.TreeNodeView(html, node, self.treeType, self.selectedPromise);
                    self.$childrenContainer.append(html);
                });

                self.expandChildren();
                self.$refreshIcon.fadeOut();
                self.childrenLoaded = true;

                if (!nodes.length) {
                    self.$expandButton.css('visibility', 'hidden');
                }
            });
        } else if (this.isExpanded) {
            this.collapseChildren();
        } else {
            this.expandChildren();
        }
    };

    uBeSocial.Views.TreeNodeView.prototype.expandChildren = function () {
        this.isExpanded = true;
        this.$expandButton.addClass('glyphicon-menu-down');
        this.$expandButton.removeClass('glyphicon-menu-right');
        this.$childrenContainer.show();
    };

    uBeSocial.Views.TreeNodeView.prototype.collapseChildren = function () {
        this.isExpanded = false;
        this.$expandButton.addClass('glyphicon-menu-right');
        this.$expandButton.removeClass('glyphicon-menu-down');
        this.$childrenContainer.hide();
    };

    uBeSocial.Views.TreeNodeView.prototype.nodeSelected = function () {
        this.selectedPromise.resolve(this.treeNode);
    };
}(uBeSocial, $));