﻿var uBeSocial = uBeSocial || { };

(function (uBeSocial, $) {
    uBeSocial.App = function () {
        this.init();
    };

    uBeSocial.App.prototype.init = function () {
        this.controllers = [];

        this.setupControllers();
    };

    uBeSocial.App.prototype.setupControllers = function () {
        var self = this;
        $('[data-controller]').each(function (index, element) {
            var $element = $(element);
            var controllerType = $element.data('controller');
            var controllerCtor = uBeSocial.Controllers[controllerType + 'Controller'];

            self.controllers.push(new controllerCtor($element));
        });
    };
}(uBeSocial, $));