﻿var uBeSocial = uBeSocial || {};
uBeSocial.Utils = uBeSocial.Utils || {};

(function (uBeSocial, $) {
    uBeSocial.Utils.TreeApi = {
        getChildNodes: function (treeType, parentId) {
            return $.get('/umbraco/ubesocial/ubesocialdashboard/getchildnodes', { treeType: treeType, id: parentId });
        }
    };
}(uBeSocial, $));