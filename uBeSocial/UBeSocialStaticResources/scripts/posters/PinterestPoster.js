﻿var uBeSocial = uBeSocial || {};
uBeSocial.Posters = uBeSocial.Posters || {};

(function (uBeSocial, $, Pinterest) {
    var _super = uBeSocial.Posters.BasePoster;

    uBeSocial.Posters.PinterestPoster = function () {
        this.init();
    };

    uBeSocial.Posters.PinterestPoster.prototype = Object.create(_super.prototype);

    uBeSocial.Posters.PinterestPoster.prototype.init = function () {
        _super.prototype.init.call(this);

        this.key = 'PINTEREST';
        this.name = 'Pinterest';

        if (this.isConfigured()) {
            Pinterest.init({
                appId: SETTINGS.PINTEREST.AppId,
                cookie: false
            });
        }

        return this;
    };

    uBeSocial.Posters.PinterestPoster.prototype.post = function (message) {
        this.promise = $.Deferred();
        var image = this.media.length > 0 ? this.media[0] : undefined;
        var link = this.content.length > 0 ? this.content[0] : undefined;

        Pinterest.pin(image, message, link, this.postedHandler);

        this.promise.resolve('Pin must be completed in the Pinterest popup.');

        return this.promise;
    };

    uBeSocial.Posters.PinterestPoster.prototype.isConfigured = function () {
        return SETTINGS && SETTINGS.PINTEREST && SETTINGS.PINTEREST.AppId;
    };
}(uBeSocial, $, PDK));