﻿var uBeSocial = uBeSocial || {};
uBeSocial.Posters = uBeSocial.Posters || {};

(function (uBeSocial, $, Facebook) {
    var _super = uBeSocial.Posters.BasePoster;

    uBeSocial.Posters.FacebookPoster = function () {
        this.init()
            .setupHandlers();
    };

    uBeSocial.Posters.FacebookPoster.prototype = Object.create(_super.prototype);

    uBeSocial.Posters.FacebookPoster.prototype.init = function () {
        _super.prototype.init.call(this);

        this.key = 'FACEBOOK';
        this.name = 'Facebook';

        if (this.isConfigured()) {
            Facebook.init({
                appId: SETTINGS.FACEBOOK.AppId,
                xfbml: true,
                version: 'v2.7'
            });
        }

        return this;
    };

    uBeSocial.Posters.FacebookPoster.prototype.setupHandlers = function () {
        this.postedHandler = this.postedHandler.bind(this);

        return this;
    };

    uBeSocial.Posters.FacebookPoster.prototype.post = function (message) {
        var self = this;

        this.promise = $.Deferred();
        Facebook.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                Facebook.api('/me/feed', 'post', { message: message }, self.postedHandler);
            } else {
                Facebook.login(function (response) {
                    if (response.authResponse) {
                        Facebook.api('/me/feed', 'post', { message: message }, self.postedHandler);
                    } else {
                        self.promise.reject('Failed to login to Facebook');
                    }
                }, { scope: 'publish_actions' });
            }
        });

        return this.promise;
    };

    uBeSocial.Posters.FacebookPoster.prototype.isConfigured = function () {
        return SETTINGS && SETTINGS.FACEBOOK && SETTINGS.FACEBOOK.AppId;
    };

    uBeSocial.Posters.FacebookPoster.prototype.postedHandler = function (response) {
        if (!response || response.error) {
            var message = !response || !response.error || !response.error.error_user_msg ? 'Unexpected error posting to Facebook' : response.error.error_user_msg;
            this.promise.reject(message)
        } else {
            this.promise.resolve('Successfully posted message to Facebook');
        }
    };
}(uBeSocial, $, FB));