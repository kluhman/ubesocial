﻿var uBeSocial = uBeSocial || {};
uBeSocial.Posters = uBeSocial.Posters || {};

(function (uBeSocial, $) {
    uBeSocial.Posters.BasePoster = function () {
        this.init();
    };

    uBeSocial.Posters.BasePoster.prototype.init = function () {
        this.media = [];
        this.content = [];
        this.promise = null;

        return this;
    };

    uBeSocial.Posters.BasePoster.prototype.addContent = function (contentUrl) {
        this.content.push(contentUrl);
    };

    uBeSocial.Posters.BasePoster.prototype.addMedia = function (mediaUrl) {
        this.media.push(mediaUrl);
    };

    uBeSocial.Posters.BasePoster.prototype.reset = function () {
        this.media.splice(0, this.media.length);
        this.content.splice(0, this.content.length);
    };

    uBeSocial.Posters.BasePoster.prototype.isConfigured = function () {
        return false;
    };
}(uBeSocial, $));