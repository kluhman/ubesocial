﻿var uBeSocial = uBeSocial || {};
uBeSocial.Posters = uBeSocial.Posters || {};

(function (uBeSocial, $) {
    var _super = uBeSocial.Posters.BasePoster;

    uBeSocial.Posters.TwitterPoster = function () {
        this.init();
    };

    uBeSocial.Posters.TwitterPoster.prototype = Object.create(_super.prototype);

    uBeSocial.Posters.TwitterPoster.prototype.init = function () {
        _super.prototype.init.call(this);

        this.key = 'TWITTER';
        this.name = 'Twitter';

        return this;
    };

    uBeSocial.Posters.TwitterPoster.prototype.post = function (message) {
        var self = this;

        this.promise = $.Deferred();
        $.post('/umbraco/uBeSocial/UBeSocialTwitter/Tweet', { tweet: message }, function (result) {
            if (result.Success) {
                self.promise.resolve(result.Message || 'Successfully posted status to Twitter.');
            } else {
                self.promise.reject(result.Message || 'An unexpected error occurred posting status to Twitter');
            }
        });

        return this.promise;
    };

    uBeSocial.Posters.TwitterPoster.prototype.isConfigured = function () {
        return true;
    };
}(uBeSocial, $));