﻿var uBeSocial = uBeSocial || {};
uBeSocial.Controllers = uBeSocial.Controllers || {};

(function (uBeSocial, $) {
    var SELECTORS = {
        FORM: '[data-ubesocial-role=form]',
        RESET: '[data-ubesocial-role=reset]',
        ATTACH: '[data-ubesocial-role=attach]',
        MESSAGE: '[data-ubesocial-role=message]',
        NETWORKS: '[data-ubesocial-role=networks]',
        ADD_MEDIA: '[data-ubesocial-role=addmedia]',
        ADD_CONTENT: '[data-ubesocial-role=addcontent]',
        STATUS_MESSAGES: '[data-ubesocial-role=statusMessages]'
    };

    var buildNetworkId = function (key) {
        return 'Networks_' + key;
    };

    var buildAlert = function (alertType, message) {
        var $alert = $('<div class="alert alert-' + alertType + ' alert-dismissable" role="alert"></div>')
        $alert.append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        $alert.append(message);

        return $alert;
    };

    uBeSocial.Controllers.uBeSocialController = function ($element) {
        this.init($element)
            .setupHandlers()
            .createChildren()
            .enable();
    };

    uBeSocial.Controllers.uBeSocialController.prototype.init = function ($element) {
        this.$element = $element;
        this.posters = [];

        return this;
    };

    uBeSocial.Controllers.uBeSocialController.prototype.setupHandlers = function () {
        this.reset = this.reset.bind(this);
        this.addMedia = this.addMedia.bind(this);
        this.addContent = this.addContent.bind(this);
        this.submitHandler = this.submitHandler.bind(this);

        return this;
    };

    uBeSocial.Controllers.uBeSocialController.prototype.createChildren = function () {
        this.$form = this.$element.find(SELECTORS.FORM);
        this.$reset = this.$element.find(SELECTORS.RESET);
        this.$message = this.$element.find(SELECTORS.MESSAGE);
        this.$addMediaButton = this.$element.find(SELECTORS.ADD_MEDIA);
        this.$addContentButton = this.$element.find(SELECTORS.ADD_CONTENT);
        this.$statusMessages = this.$element.find(SELECTORS.STATUS_MESSAGES);
        this.$networksContainer = this.$element.find(SELECTORS.NETWORKS);

        this.attachContentModal = new uBeSocial.Controllers.AttachContentModalController(this.$element.find(SELECTORS.ATTACH));

        for (var posterName in uBeSocial.Posters) {
            if (uBeSocial.Posters.hasOwnProperty(posterName)) {
                var poster = new uBeSocial.Posters[posterName];
                if (poster.isConfigured()) {
                    this.posters.push(poster);

                    var label = $('<label class="checkbox-inline"></label>');
                    var checkbox = $('<input type="checkbox" name="Networks" />');
                    var checkboxId = buildNetworkId(poster.key);

                    label.attr('for', checkboxId);
                    checkbox.attr('id', checkboxId);
                    checkbox.attr('value', poster.key);
                    label.append(checkbox);
                    label.append(poster.name);

                    this.$networksContainer.append(label);
                }
            }
        }

        return this;
    };

    uBeSocial.Controllers.uBeSocialController.prototype.enable = function () {
        this.$reset.on('click', this.reset);
        this.$form.on('submit', this.submitHandler);
        this.$addMediaButton.on('click', this.addMedia);
        this.$addContentButton.on('click', this.addContent);

        return this;
    };

    uBeSocial.Controllers.uBeSocialController.prototype.submitHandler = function (e) {
        e.preventDefault();

        var self = this;

        var postPromises = [];
        this.posters.forEach(function (poster) {
            var $posterCheckbox = self.$networksContainer.find('#' + buildNetworkId(poster.key));
            if ($posterCheckbox.prop('checked')) {
                var promise = poster.post(self.$message.val()).done(function (message) {
                    $posterCheckbox.prop('checked', false);
                    self.$statusMessages.append(buildAlert('success', message));
                }).fail(function (message) {
                    self.$statusMessages.append(buildAlert('danger', message));
                });

                postPromises.push(promise);
            }
        });

        $.when(postPromises).done(function () {
            self.reset();
        });
    };

    uBeSocial.Controllers.uBeSocialController.prototype.reset = function() {
        var self = this;
        this.$message.val('');
        this.posters.forEach(function (poster) {
            var $posterCheckbox = self.$networksContainer.find('#' + buildNetworkId(poster.key));
            $posterCheckbox.prop('checked', false);
            poster.reset();
        });
    };

    uBeSocial.Controllers.uBeSocialController.prototype.addMedia = function () {
        var self = this;
        var promise = $.Deferred();
        this.attachContentModal.open('media', promise.done(function (treeNode) {
            self.attachContentModal.close();
            self.$message.val(self.$message.val() + treeNode.Url);
            self.posters.forEach(function (poster) {
                poster.addMedia(treeNode.Url);
            });
        }));
    };

    uBeSocial.Controllers.uBeSocialController.prototype.addContent = function () {
        var self = this;
        var promise = $.Deferred();
        this.attachContentModal.open('content', promise.done(function (treeNode) {
            self.attachContentModal.close();
            self.$message.val(self.$message.val() + treeNode.Url);
            self.posters.forEach(function (poster) {
                poster.addContent(treeNode.Url);
            });
        }));
    };
}(uBeSocial, $));