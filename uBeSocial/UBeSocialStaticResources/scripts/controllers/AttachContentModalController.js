﻿var uBeSocial = uBeSocial || {};
uBeSocial.Controllers = uBeSocial.Controllers || {};

(function (uBeSocial, $) {
    var SELECTORS = {
        CONTENT_CONTAINER: '[data-addcontent-role=content]',
        TREE_NODE_TEMPLATE: '[data-treenode-role=template]'
    };

    uBeSocial.Controllers.AttachContentModalController = function ($element) {
        this.init($element)
            .createChildren();
    };

    uBeSocial.Controllers.AttachContentModalController.prototype.init = function ($element) {
        this.promise = null;
        this.$element = $element;

        return this;
    };

    uBeSocial.Controllers.AttachContentModalController.prototype.createChildren = function () {
        this.$contentContainer = this.$element.find(SELECTORS.CONTENT_CONTAINER);
        this.$treeNodeTemplate = this.$element.find(SELECTORS.TREE_NODE_TEMPLATE).children().first();

        return this;
    };

    uBeSocial.Controllers.AttachContentModalController.prototype.open = function (treeType, promise) {
        var self = this;
        this.promise = promise;
        this.$element.modal('show');

        this.$contentContainer.empty();

        var $treeElement = this.$treeNodeTemplate.clone();
        var view = new uBeSocial.Views.TreeNodeView($treeElement, { Id: -1, Name: treeType.toUpperCase(), Url: null }, treeType, promise);
        this.$contentContainer.append($treeElement);
    };

    uBeSocial.Controllers.AttachContentModalController.prototype.close = function (treeType, promise) {
        this.$element.modal('hide');
    };
}(uBeSocial, $));