﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Hosting;

using uBeSocial.Helpers;

namespace uBeSocial.VirtualProviders
{
    internal class EmbeddedResourceVirtualFile : VirtualFile
    {
        private readonly string _virtualPath;
        private readonly Assembly _executingAssembly;

        public EmbeddedResourceVirtualFile(string virtualPath) : base(virtualPath)
        {
            _virtualPath = virtualPath;
            _executingAssembly = Assembly.GetExecutingAssembly();
        }

        public override Stream Open()
        {
            var path = _virtualPath;
            if (path.StartsWith("~", StringComparison.Ordinal))
            {
                path = path.Substring(1);
            }
            if (!path.StartsWith("/", StringComparison.Ordinal))
            {
                path = string.Concat("/", path);
            }

            return EmbeddedResourceHelper.GetEmbeddedResource(_executingAssembly, EmbeddedResourceHelper.GetEmbeddedResourceName(_executingAssembly, path));
        }
    }
}