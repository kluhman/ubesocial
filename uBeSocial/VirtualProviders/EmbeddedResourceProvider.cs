﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Caching;
using System.Web.Hosting;

using uBeSocial.Helpers;

namespace uBeSocial.VirtualProviders
{
    internal class EmbeddedResourceProvider : VirtualPathProvider
    {
        private readonly Assembly _executingAssembly;

        public EmbeddedResourceProvider()
        {
            _executingAssembly = Assembly.GetExecutingAssembly();
        }

        public override bool FileExists(string virtualPath)
        {
            return IsEmbeddedResourcePath(virtualPath) || base.FileExists(virtualPath);
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            return IsEmbeddedResourcePath(virtualPath) ? new EmbeddedResourceVirtualFile(virtualPath) : base.GetFile(virtualPath);
        }

        public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
        {
            return IsEmbeddedResourcePath(virtualPath) ? null : base.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
        }

        private bool IsEmbeddedResourcePath(string virtualPath)
        {
            var assemblyResourceNames = _executingAssembly.GetManifestResourceNames();
            var resourceName = EmbeddedResourceHelper.GetEmbeddedResourceName(_executingAssembly, virtualPath);
            return assemblyResourceNames.Contains(resourceName, StringComparer.OrdinalIgnoreCase);
        }
    }
}