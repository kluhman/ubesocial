![uBeSocial](assets/uBeSocial-logo.png)

# uBeSocial

[![NuGet](https://img.shields.io/nuget/v/uBeSocial.svg?style=flat-square)](https://www.nuget.org/packages/uBeSocial)
[![Build status](https://ci.appveyor.com/api/projects/status/645fvqmi5cf5dvv7/branch/master?svg=true)](https://ci.appveyor.com/project/kluhman/ubesocial/branch/master)

uBeSocial is custom section for the Umbraco Back-Office that enables users to post messages to all of their major social media networks (Facebook, Twitter, Pinterest, etc..) at once. uBeSocial also allows users to add links to Umbraco content and media to their social media posts.

uBeSocial has been tested on Umbraco v6.2.5. I intend to get it compatible with Umbraco v7.0+ as well. 

## Installation

uBeSocial can be installed via Nuget. This is the recommended way to install uBeSocial as it will modify your web.config and other various configurations files automatically during the install process. Make sure to include the `umbraco` directory in your web project or some of the config transformations will not work.

If you did not have the `umbraco` directory included in your project when you installed the package, or config transforms failed for whatever reason. The following are the config updates you need to make.

__Web.config__

```xml
<location path="UBeSocialStaticResources">
	<system.webServer>
	  <handlers accessPolicy="Read, Write, Script, Execute">
	    <!-- uBeSocial Static Resources-->
	    <add name="uBeSocialResources" path="*.*" verb="GET,HEAD,POST" type="System.Web.StaticFileHandler" modules="ManagedPipelineHandler" resourceType="Unspecified" />
	  </handlers>
	</system.webServer>
</location>
```

__umbraco/Config/Create/UI.xml__

```xml
<nodeType alias="uBeSocialTwitterUser">
	<header>Create Twitter User</header>
	<usercontrol>/Controls/CreateTwitterUser.ascx</usercontrol>
	<tasks>
	  <create assembly="uBeSocial" type="Trees.Tasks.TwitterUserTasks" />
	  <delete assembly="uBeSocial" type="Trees.Tasks.TwitterUserTasks" />
	</tasks>
</nodeType>
```

## Usage

Once you have installed uBeSocial, it will bootstrap itself when Umbraco starts up. uBeSocial has 3 built in social media posters (Facebook, Twitter, and Pinterest).

### Facebook Configuration

In order to use the Facebook poster, simply add your Facebook App Id to the web.config. This setting will have been added for you already if you used Nuget to install uBeSocial.

```xml
<add key="uBeSocial.Facebook.AppId" value="[Your app id here]" />
```

### Pinterest Configuration

In order to use the Pinterest poster, simply add your Pinterest App Id to the web.config. This setting will have been added for you already if you used Nuget to install uBeSocial.

```xml
<add key="uBeSocial.Pinterest.AppId" value="[Your app id here]" />
```

### Twitter Configuration

In order to use the Twitter poster, first add the following app setting to the web.config. This setting will have been added for you already if you used Nuget to install uBeSocial.

```xml
<add key="uBeSocial.Twitter.EnableTwitterPoster" value="true" />
```

Because Twitter does not have a JavaScript OAuth implementation like Facebook and Pinterest, credentials are supplied to the poster via the back-office. Launch Umbraco and navigate to the uBeSocial section of the back office. In the tree, you will notice a node called "Twitter". Right click to create a new Twitter user which will be able to post content to it's timeline using uBeSocial. You will need to enter the Consumer Key, Consumer Key Secret, Access Token, and Access Token Secret. The Consumer Key and Consumer Key secret can be obtained from your Twitter application settings. You will need to create the Access Token and Access Token Secret for your user once you have added the user to the application.

You may create as many users as you'd like in the back-office, but you can only post to one account at a time. To select the active user account, click on the "Twitter" node and select/save the active user in the dropdown. 

__NOTE: All users are stored in the database, and no credentials are passed back to the client. All credentials usage is done securely on the server.__

### Extending uBeSocial

If you don't like the functionality for the default social media posters, or you would like to include posters for more social media networks, uBeSocial can be extended quite simply. 

First create a JavaScript poster that inherits from `uBeSocial.Posters.BasePoster` (this script is guaranteed to run before your poster is added). In your poster initializtion, be sure to set the 'key' and 'name' properties of your poster.

```js
this.key = 'YOUR_POSTER_KEY';
this.name = 'Your Poster name';
```

Your custom poster should implement 2 methods, 'post' and 'isConfigured'. The signatures for these are as follows.

```js
yourPoster.prototype.post = function (message) {
	var promise = $.Deferred();
	
	//do something to post, be sure to resolve promise when done

	return promise;
};

yourPoster.prototype.isConfigured = function () {
	//this is used to ensure poster has everything is needs to run properly
	return true;
};
```
Once you have created your poster, you need to register it with uBeSocial. This can be done with a single call during application startup.

```csharp
UBeSocialApplication.RegisterNetworkPoster(new NetworkPosterRegistration
{
    Dependencies = new List<string> { "//connect.facebook.net/en_US/sdk.js" },
    Poster = "~/scripts/posters/YourPoster.js",
    SettingsName = "YOUR_POSTER_SETTINGS",
    Settings = new
    {
        AppId = "MY_APP_ID"
    }
});
```

The Dependencies property can be used to specify any scripts your poster may be dependent on, internal or external. The Poster propery is the path to your custom poster. The SettingsName and Settings property provide a way to pass settings to your custom poster as a JavaScript object. All properties in the "Settings" property will be serialized and stored in the JavaScript global object SETTINGS object. In the above example, you could find all your settings as follows:

```js
var mySettings = SETTINGS["YOUR_POSTER_SETTINGS"];
var appId = mySettings.AppId;
```

## Contributing

uBeSocial is open source and any help is appreciated. See the following sections for information on how to get involved.

### Environment Setup

In order to build and run uBeSocial, you will need the following:

1. Visual Studio 2015
2. Local IIS

To setup local IIS:

1. Open IIS Manager
2. Create a new site called "ubesocial.local" and bind it to "ubesocial.local"
3. Point the physical path to the uBeSocial project folder
4. Update your hosts file to point "ubesocial.local" to 127.0.0.1
5. Build the uBeSocial solution

### Git Methodologies

This repo uses the Git Flow methodology. To make changes, pull latest, and create a feature branch off of develop. When you are done with your changes, submit a pull request. Your PR will be reviewed ASAP and merged into develop. Commits into develop will automatically trigger a build and beta nuget package release. Once the beta package has been tested, I will merge up to master which will automatically trigger a new nuget package release.

__IMPORTANT: Before you submit your PR, please update the version in appveyor.yml appropriately. The version should follow semantic versioning standards {Major}.{Minor}.{Revision}__

### Issue Management

New bugs and improvements can be logged and tracked [here](https://bitbucket.org/kluhman/ubesocial/issues). 

